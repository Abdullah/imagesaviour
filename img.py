#!/usr/bin/env python3
# coding: utf-8
"""
Pictures taken from phones and cameras are usually have large in size.
This script will get some files from a directory and saves them back
but with file size decreased.

 ▓▓▓▓▓▓▓▓▓▓
░▓ Author ▓ Abdullah Khabir <https://abdullah.solutions>
░▓▓▓▓▓▓▓▓▓▓
░░░░░░░░░░

The MIT License (MIT)
Copyright (c) 2019 Abdullah <abdullah@abdullah.today>
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

import os
from PIL import Image

directory = input("Enter directory: ")
compression_quality = int(input("Enter compression quality (0-100): "))

if directory == '.':
    directory = os.getcwd()

os.makedirs(os.path.join(directory, 'compressed_images'), exist_ok=True)

for filename in os.listdir(directory):
    if filename.endswith('.jpg') or filename.endswith('.png') or filename.endswith('.jpeg'):
        image_path = os.path.join(directory, filename)
        with Image.open(image_path) as im:
            compressed_image_path = os.path.join(directory, 'compressed_images', filename)
            im.save(compressed_image_path, optimize=True, quality=compression_quality)
            print(f"Compressed {image_path} -> {compressed_image_path}")
